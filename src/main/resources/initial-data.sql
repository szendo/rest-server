CREATE TABLE albums (
  id     BIGINT       NOT NULL PRIMARY KEY AUTO_INCREMENT,
  name   VARCHAR(255) NOT NULL,
  artist VARCHAR(255) NOT NULL
);

CREATE TABLE tracks (
  id       BIGINT       NOT NULL PRIMARY KEY AUTO_INCREMENT,
  name     VARCHAR(255) NOT NULL,
  album_id BIGINT       NOT NULL,
  CONSTRAINT FK_album_id FOREIGN KEY (album_id) REFERENCES albums (id) ON DELETE CASCADE
);

INSERT INTO albums (name, artist) VALUES
  ('The Connection', 'Papa Roach');

SET @album_id = IDENTITY();

INSERT INTO tracks (name, album_id) VALUES
  ('Engage', @album_id),
  ('Still Swingin''', @album_id),
  ('Where Did the Angels Go', @album_id),
  ('Silence Is the Enemy', @album_id),
  ('Before I Die', @album_id),
  ('Wish You Never Met Me', @album_id),
  ('Give Back My Life', @album_id),
  ('Breathe You In', @album_id),
  ('Leader of the Broken Hearts', @album_id),
  ('Not That Beautiful', @album_id),
  ('Walking Dead', @album_id),
  ('Won''t Let Up', @album_id),
  ('As Far as I Remember', @album_id);
