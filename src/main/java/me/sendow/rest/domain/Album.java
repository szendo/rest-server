package me.sendow.rest.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.hateoas.Identifiable;

@SuppressWarnings("UnusedDeclaration")
public class Album implements Identifiable<Long> {
    private Long id;
    @NotEmpty
    private String name;
    @NotEmpty
    private String artist;

    public Album() {
    }

    public Album(Long id, String name, String artist) {
        this.id = id;
        this.name = name;
        this.artist = artist;
    }

    @JsonProperty
    public Long getId() {
        return id;
    }

    @JsonIgnore
    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }
}
