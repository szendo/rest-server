package me.sendow.rest.domain.assembler.impl;

import me.sendow.rest.domain.Album;
import me.sendow.rest.domain.assembler.AlbumResourceAssembler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityLinks;
import org.springframework.hateoas.Resource;
import org.springframework.stereotype.Component;

@Component
public class AlbumResourceAssemblerImpl implements AlbumResourceAssembler {

    @Autowired
    private EntityLinks entityLinks;

    @Override
    public Resource<Album> toResource(Album entity) {
        return new Resource<>(entity,
                entityLinks.linkToSingleResource(entity),
                entityLinks.linkForSingleResource(entity).slash("tracks").withRel("tracks"));
    }

}
