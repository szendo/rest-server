package me.sendow.rest.domain.assembler.impl;

import me.sendow.rest.domain.Album;
import me.sendow.rest.domain.Track;
import me.sendow.rest.domain.assembler.TrackResourceAssembler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityLinks;
import org.springframework.hateoas.Resource;
import org.springframework.stereotype.Component;

@Component
public class TrackResourceAssemblerImpl implements TrackResourceAssembler {

    @Autowired
    private EntityLinks entityLinks;

    @Override
    public Resource<Track> toResource(Track entity) {
        return new Resource<>(entity,
                entityLinks.linkToSingleResource(entity),
                entityLinks.linkForSingleResource(Album.class, entity.getAlbumId()).withRel("album"));
    }

}
