package me.sendow.rest.domain.assembler;

import me.sendow.rest.domain.Album;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceAssembler;

public interface AlbumResourceAssembler extends ResourceAssembler<Album, Resource<Album>> {
    Resource<Album> toResource(Album entity);
}
