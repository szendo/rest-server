package me.sendow.rest.domain.assembler;

import me.sendow.rest.domain.Track;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceAssembler;

public interface TrackResourceAssembler extends ResourceAssembler<Track, Resource<Track>> {
    Resource<Track> toResource(Track entity);
}
