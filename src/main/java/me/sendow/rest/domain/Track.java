package me.sendow.rest.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.hateoas.Identifiable;

import javax.validation.constraints.NotNull;

@SuppressWarnings("UnusedDeclaration")
public class Track implements Identifiable<Long> {
    private Long id;
    @NotEmpty
    private String name;
    @JsonProperty(required = false)
    private Long albumId;

    public Track() {
    }

    public Track(Long id, String name, Long albumId) {
        this.id = id;
        this.name = name;
        this.albumId = albumId;
    }

    @JsonProperty
    public Long getId() {
        return id;
    }

    @JsonIgnore()
    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getAlbumId() {
        return albumId;
    }

    public void setAlbumId(Long albumId) {
        this.albumId = albumId;
    }
}