package me.sendow.rest.controller;

import me.sendow.rest.domain.Track;
import me.sendow.rest.domain.assembler.TrackResourceAssembler;
import me.sendow.rest.repository.AlbumRepository;
import me.sendow.rest.repository.TrackRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.ExposesResourceFor;
import org.springframework.hateoas.PagedResources;
import org.springframework.hateoas.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;
import java.net.URI;

@Controller
@ExposesResourceFor(Track.class)
@RequestMapping("/tracks")
public class TrackController {

    @Autowired
    private TrackRepository trackRepository;

    @Autowired
    private AlbumRepository albumRepository;

    @Autowired
    private PagedResourcesAssembler<Track> pagedTrackResourceAssembler;

    @Autowired
    private TrackResourceAssembler trackResourceAssembler;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<PagedResources<Resource<Track>>> listTracks(
            @PageableDefault(size = 10) Pageable pageable) {

        final Page<Track> trackPage = trackRepository.findAll(pageable);

        return new ResponseEntity<>(pagedTrackResourceAssembler.toResource(trackPage, trackResourceAssembler), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Void> createTrack(
            @RequestBody @Valid Track track) {

        if (albumRepository.exists(track.getAlbumId())) {
            Track savedTrack = trackRepository.save(track);

            HttpHeaders headers = new HttpHeaders();
            headers.setLocation(URI.create(trackResourceAssembler.toResource(savedTrack).getId().getHref()));
            return new ResponseEntity<>(headers, HttpStatus.CREATED);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/{trackId}", method = RequestMethod.GET)
    public ResponseEntity<Resource<Track>> getTrack(
            @PathVariable("trackId") Long trackId) {

        final Track track = trackRepository.findOne(trackId);

        if (track != null) {
            return new ResponseEntity<>(trackResourceAssembler.toResource(track), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/{trackId}", method = RequestMethod.PUT)
    public ResponseEntity<Resource<Track>> editTrack(
            @PathVariable("trackId") Long trackId,
            @RequestBody @Valid Track track) {

        if (track.getAlbumId() != null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        Track oldTrack = trackRepository.findOne(trackId);
        track.setAlbumId(oldTrack.getAlbumId());

        track.setId(trackId);
        Track savedTrack = trackRepository.save(track);

        if (savedTrack != null) {
            return new ResponseEntity<>(trackResourceAssembler.toResource(savedTrack), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/{trackId}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> deleteTrack(
            @PathVariable("trackId") Long trackId) {

        final Track track = trackRepository.findOne(trackId);

        if (track != null) {
            trackRepository.delete(track.getId());
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

}
