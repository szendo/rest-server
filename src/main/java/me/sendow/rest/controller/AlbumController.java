package me.sendow.rest.controller;

import me.sendow.rest.domain.Album;
import me.sendow.rest.domain.Track;
import me.sendow.rest.domain.assembler.AlbumResourceAssembler;
import me.sendow.rest.domain.assembler.TrackResourceAssembler;
import me.sendow.rest.repository.AlbumRepository;
import me.sendow.rest.repository.TrackRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.ExposesResourceFor;
import org.springframework.hateoas.PagedResources;
import org.springframework.hateoas.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@ExposesResourceFor(Album.class)
@RequestMapping("/albums")
public class AlbumController {

    @Autowired
    private AlbumRepository albumRepository;

    @Autowired
    private TrackRepository trackRepository;

    @Autowired
    private PagedResourcesAssembler<Album> pagedAlbumResourceAssembler;

    @Autowired
    private AlbumResourceAssembler albumResourceAssembler;

    @Autowired
    private TrackResourceAssembler trackResourceAssembler;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<PagedResources<Resource<Album>>> listAlbums(
            @PageableDefault(size = 10) Pageable pageable) {

        final Page<Album> albumPage = albumRepository.findAll(pageable);

        return new ResponseEntity<>(pagedAlbumResourceAssembler.toResource(albumPage, albumResourceAssembler), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Void> createAlbum(
            @RequestBody @Valid Album album) {

        Album savedAlbum = albumRepository.save(album);

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(URI.create(albumResourceAssembler.toResource(savedAlbum).getId().getHref()));
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/{albumId}", method = RequestMethod.GET)
    public ResponseEntity<Resource<Album>> getAlbum(
            @PathVariable("albumId") Long albumId) {

        final Album album = albumRepository.findOne(albumId);

        if (album != null) {
            return new ResponseEntity<>(albumResourceAssembler.toResource(album), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/{albumId}", method = RequestMethod.PUT)
    public ResponseEntity<Resource<Album>> editAlbum(
            @PathVariable("albumId") Long albumId,
            @RequestBody @Valid Album album) {

        album.setId(albumId);
        Album savedAlbum = albumRepository.save(album);

        if (savedAlbum != null) {
            return new ResponseEntity<>(albumResourceAssembler.toResource(savedAlbum), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/{albumId}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> deleteAlbum(
            @PathVariable("albumId") Long albumId) {

        final Album album = albumRepository.findOne(albumId);

        if (album != null) {
            albumRepository.delete(album.getId());
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/{albumId}/tracks", method = RequestMethod.GET)
    public ResponseEntity<List<Resource<Track>>> getAlbumTracks(
            @PathVariable("albumId") Long albumId) {

        if (albumRepository.exists(albumId)) {
            List<Track> tracks = trackRepository.findByAlbumId(albumId);
            return new ResponseEntity<>(
                    tracks.stream()
                            .map(trackResourceAssembler::toResource)
                            .collect(Collectors.toList()),
                    HttpStatus.OK
            );
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/{albumId}/tracks", method = RequestMethod.POST)
    public ResponseEntity<Void> createTrackInAlbum(
            @PathVariable("albumId") Long albumId,
            @RequestBody @Valid Track track) {

        if (albumRepository.exists(albumId)) {
            track.setAlbumId(albumId);
            Track savedTrack = trackRepository.save(track);

            HttpHeaders headers = new HttpHeaders();
            headers.setLocation(URI.create(trackResourceAssembler.toResource(savedTrack).getId().getHref()));
            return new ResponseEntity<>(headers, HttpStatus.CREATED);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

}
