package me.sendow.rest.repository;

import me.sendow.rest.domain.Track;

import java.util.List;

public interface TrackRepository extends Repository<Track, Long> {

    List<Track> findByAlbumId(Long albumId);

}
