package me.sendow.rest.repository;

import me.sendow.rest.domain.Album;

public interface AlbumRepository extends Repository<Album, Long> {
}
