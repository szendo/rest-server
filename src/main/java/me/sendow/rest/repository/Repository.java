package me.sendow.rest.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.Identifiable;

import java.io.Serializable;
import java.util.List;

public interface Repository<T extends Identifiable<ID>, ID extends Serializable> {

    long count();

    void delete(ID id);

    boolean exists(ID id);

    List<T> findAll();

    Page<T> findAll(Pageable pageable);

    T findOne(ID id);

    T save(T entity);

}