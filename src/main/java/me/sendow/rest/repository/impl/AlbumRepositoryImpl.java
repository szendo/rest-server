package me.sendow.rest.repository.impl;

import me.sendow.rest.domain.Album;
import me.sendow.rest.repository.AlbumRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.List;

@Repository
public class AlbumRepositoryImpl implements AlbumRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public long count() {
        return jdbcTemplate.queryForObject("SELECT COUNT(*) FROM albums", Long.class);
    }

    @Override
    public void delete(Long id) {
        jdbcTemplate.update("DELETE FROM albums WHERE id = ?", id);
    }

    @Override
    public boolean exists(Long id) {
        return jdbcTemplate.queryForObject("SELECT COUNT(*) FROM albums WHERE id = ?", Long.class, id) > 0;
    }

    @Override
    public List<Album> findAll() {
        return jdbcTemplate.query("SELECT id, name, artist FROM albums ORDER BY id ASC",
                BeanPropertyRowMapper.newInstance(Album.class));
    }

    @Override
    public Page<Album> findAll(Pageable pageable) {
        long total = count();
        List<Album> albumList = jdbcTemplate.query(
                "SELECT id, name, artist FROM albums ORDER BY id ASC LIMIT ? OFFSET ? ",
                BeanPropertyRowMapper.newInstance(Album.class),
                pageable.getPageSize(),
                pageable.getOffset()
        );
        return new PageImpl<>(albumList, pageable, total);
    }

    @Override
    public Album findOne(Long id) {
        try {
            return jdbcTemplate.queryForObject(
                    "SELECT id, name, artist FROM albums WHERE id = ?",
                    BeanPropertyRowMapper.newInstance(Album.class),
                    id);
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    @Override
    public Album save(Album entity) {
        Long id = entity.getId();
        if (id == null) {
            GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();
            jdbcTemplate.update(con -> {
                PreparedStatement ps = con.prepareStatement("INSERT INTO albums (name, artist) VALUES (?, ?)",
                        Statement.RETURN_GENERATED_KEYS);
                ps.setString(1, entity.getName());
                ps.setString(2, entity.getArtist());
                return ps;
            }, keyHolder);
            id = keyHolder.getKey().longValue();
        } else {
            jdbcTemplate.update("UPDATE albums SET name = ?, artist = ? WHERE id = ?",
                    entity.getName(), entity.getArtist(), entity.getId());
        }
        return findOne(id);
    }

}
