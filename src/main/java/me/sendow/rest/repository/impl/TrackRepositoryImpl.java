package me.sendow.rest.repository.impl;

import me.sendow.rest.domain.Track;
import me.sendow.rest.repository.TrackRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.List;

@Repository
public class TrackRepositoryImpl implements TrackRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public long count() {
        return jdbcTemplate.queryForObject("SELECT COUNT(*) FROM tracks", Long.class);
    }

    @Override
    public void delete(Long id) {
        jdbcTemplate.update("DELETE FROM tracks WHERE id = ?", id);
    }

    @Override
    public boolean exists(Long id) {
        return jdbcTemplate.queryForObject("SELECT COUNT(*) FROM tracks WHERE id = ?", Long.class, id) > 0;
    }

    @Override
    public List<Track> findAll() {
        return jdbcTemplate.query("SELECT id, name, album_id FROM tracks ORDER BY id ASC",
                BeanPropertyRowMapper.newInstance(Track.class));
    }

    @Override
    public Page<Track> findAll(Pageable pageable) {
        long total = count();
        List<Track> trackList = jdbcTemplate.query(
                "SELECT id, name, album_id FROM tracks ORDER BY id ASC LIMIT ? OFFSET ? ",
                BeanPropertyRowMapper.newInstance(Track.class),
                pageable.getPageSize(),
                pageable.getOffset()
        );
        return new PageImpl<>(trackList, pageable, total);
    }

    @Override
    public List<Track> findByAlbumId(Long albumId) {
        return jdbcTemplate.query(
                "SELECT id, name, album_id FROM tracks WHERE album_id = ? ORDER BY id ASC ",
                BeanPropertyRowMapper.newInstance(Track.class),
                albumId
        );
    }

    @Override
    public Track findOne(Long id) {
        try {
            return jdbcTemplate.queryForObject(
                    "SELECT id, name, album_id FROM tracks WHERE id = ?",
                    BeanPropertyRowMapper.newInstance(Track.class),
                    id);
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    @Override
    public Track save(Track entity) {
        Long id = entity.getId();
        if (id == null) {
            GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();
            jdbcTemplate.update(con -> {
                PreparedStatement ps = con.prepareStatement("INSERT INTO tracks (name, album_id) VALUES (?, ?)",
                        Statement.RETURN_GENERATED_KEYS);
                ps.setString(1, entity.getName());
                ps.setLong(2, entity.getAlbumId());
                return ps;
            }, keyHolder);
            id = keyHolder.getKey().longValue();
        } else {
            jdbcTemplate.update(
                    "UPDATE tracks SET name = ?, album_id = ? WHERE id = ?",
                    entity.getName(), entity.getAlbumId(), entity.getId());
        }
        return findOne(id);
    }
}
